# Function dependencies, for example:
# package>=version
pymongo==3.6.1
json-encoder==0.4.4
DateTime==4.3
Flask==1.0.3
requests==2.18.4
google-cloud==0.34.0
google-cloud-firestore==1.2.0
