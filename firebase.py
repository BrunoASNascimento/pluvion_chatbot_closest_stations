from google.cloud import firestore


def get_station_fs(collection_name, lat_max, lat_min, lon_max, lon_min):
    db = firestore.Client()
    doc = db.collection(collection_name).where(
        'latitude', '>=', lat_min).where('latitude', '<=', lat_max)
    docs = doc.stream()
    # print(docs)
    listStations = []
    for doc in docs:
        data = doc.to_dict()
        listStations.append(
            [
                data['hwid'],
                data['longitude'],
                data['latitude']
            ]
        )

    if listStations == []:
        data = {
            "status": False,
            "data": listStations
        }
    else:
        data = {
            "status": True,
            "data": listStations
        }

    return data


# lat = -9.6336995
# lon = -35.7382999
# step = 0.025
# print(get_station_fs('stations', lat+step, lat-step, lon+step, lon-step))
