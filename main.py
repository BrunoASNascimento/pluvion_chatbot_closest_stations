from pymongo import MongoClient, GEO2D
import ssl
import requests
import datetime
from flask import jsonify
from math import radians, cos, sin, asin, sqrt
from bson.son import SON
import json
import pytz
import firebase
import os

mongo_url = os.environ.get(
    'MONGO_URL',
    'Specified environment variable is not set.'
)
step = 0.025


def haversine(lon1, lat1, lon2, lat2):

    lat1 = float(lat1)
    lat2 = float(lat2)
    lon1 = float(lon1)
    lon2 = float(lon2)

    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a))
    r = 6371  # Radius of earth in kilometers. Use 3956 for miles
    return c * r


def closest_stations(lat, lon):
    distance = 1
    #print("start control_st",control_st)
    listStations = []
    while (distance <= 5 and listStations == []):
        client = MongoClient(mongo_url, ssl_cert_reqs=ssl.CERT_NONE)
        db = client['api-wtr']
        db.stations.create_index([("coordinates", GEO2D)])
        geo_query = {"coordinates": SON(
            [("$near", [float(lon), float(lat)]), ("$maxDistance", float(distance)/111.12)])}
        cursor = db.stations.find(geo_query)

        for idx, val in enumerate(cursor):
            listStations.append(
                [val['stationHWID'],
                 haversine(lon, lat, val['coordinates'][0], val['coordinates'][1])]
            )
        print('st', listStations, distance)
        distance = distance + 1
        #print("end control_st",control_st)
    if listStations == []:
        print('Try Firestore ...')
        data = firebase.get_station_fs(
            'stations',
            lat+step,
            lat-step,
            lon+step,
            lon-step
        )
        listStations = []
        try:
            for st in data['listStations']:
                listStations.append(
                    [st[0],
                     haversine(lon, lat, st[1], st[2])]
                )
        except:
            data = {
                "status": False,
                "data": listStations
            }
    else:
        data = {
            "status": True,
            "data": listStations
        }
    return data


def pluvion_chatbot_closest_stations(request):

    json_req = request.args.to_dict(flat=False)

    # url/?lat=value&lon=value
    lat = (json_req['lat'])[0]
    lon = (json_req['lon'])[0]
    print(lat, lon)
    if request.method == 'OPTIONS':
        # Allows GET requests from any origin with the Content-Type
        # header and caches preflight response for an 3600s
        headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Max-Age': '3600'
        }

        return ('', 204, headers)

    # Set CORS headers for the main request
    headers = {
        'Access-Control-Allow-Origin': '*'
    }

    try:
        data = closest_stations(lat, lon)
    except:
        data = {"status": False}

    return (jsonify(data), 200, headers)


# lat = -22.819521
# lon = -47.044802
# print(closest_stations(lat, lon))
